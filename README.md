# 目前兼容 thinkPHP6.0 、Laravel8+

# 安装方式
 1. `composer require slyue/project-validation-client`  安装验证扩展包
 2.  在index.php的类自动加载机制以后，或全局中间件等位置，写以下代码 `(new \Slyue\ProjectValidationClient\Main())->setNumber('230315')->check();`
 3.  上面的`setNumber` 换成管理后台你自己的项目编号


#使用示例
## ThinkPHP
- index.php入口文件中添加代码
```namespace think;

require __DIR__ . '/../vendor/autoload.php';

// 执行HTTP应用并响应
$http = (new App())->http;

$response = $http->run();
//此处插入代码
(new \Slyue\ProjectValidationClient\Main())->setNumber('230315')->check();
$response->send();

$http->end($response);


```
大功告成
